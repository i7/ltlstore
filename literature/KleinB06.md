---
title: 'Experiments with deterministic ω-automata for formulas of linear temporal logic'
year: '2006'
citekey: 'DBLP:journals/tcs/KleinB06'
used_sets:
- '[Spec Patterns](http://patterns.projects.cs.ksu.edu/documentation/patterns/ltl.shtml)'
---

# Abstract

The paper tackles the problem of generating deterministic automata from LTL specifications. To this end, several improvements to Safra's determinization procedure are presented.

# Formulas

The following previouly unpublished formulas are defined:

* AND_{i=0}^n (GFa_i → GFb_i)
