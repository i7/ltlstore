---
title: 'Minimal Büchi Automata for Certain Classes of LTL Formulas'
year: '2009'
citekey: 'DBLP:conf/depcos/CichonCJ09'
---

# Abstract

This paper lists the minimal Büchi automaton for several frequently used formulas in model checking.

# Formulas

The following previouly unpublished formulas are defined:

* F(p1 ∧ F(p2 ∧ F(p3 ∧ ... F pn) ... )) ∧ F(q_1 ∧ F(q_2 ∧ F(q_3 ∧ ... F q_n) ... ))
* F(p ∧ X(p ∧ X(p ∧ ... Xp) ... )) ∧ F(q ∧ X(q ∧ X(q ∧ ... Xq) ... ))
* F(p ∧ Xp ∧ X^2p ∧ ... X^n p) ∧ F(q ∧ Xq ∧ X^2 q ∧ ... X^n q)
* AND_{i=0}^n GF p_i
* OR_{i=0}^n FG p_i
