---
title: 'Larger Automata and Less Work for LTL Model Checking'
year: '2006'
citekey: 'DBLP:conf/spin/GeldenhuysH06'
used_sets: 
- 'DBLP:conf/fmsp/DwyerAC98'
- 'DBLP:conf/cav/SomenziB00'
- 'DBLP:conf/concur/EtessamiH00'
---

# Abstract

The paper compares different approaches to model checking. For the evaluation the authors randomly generate formulas and collect several formulas from the literature.

# Formulas

No new formulas (except randomly generated formulas) are listed.
