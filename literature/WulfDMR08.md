---
title: 'Antichains: Alternative Algorithms for LTL Satisfiability and Model-Checking'
year: '2008'
citekey: 'DBLP:conf/tacas/WulfDMR08'
---

# Abstract

The paper (@DBLP:conf/tacas/WulfDMR08) propose novel antichain-based algorihtms for LTL satisfiability and model-checking which work directly on alternating automata. The paper evaluates the tool on existing model checking sets.

# Formulas

The following formula sets (or a subset) were used: 
* described in a technical report, not available anymore