---
title: 'Comparison of LTL to Deterministic Rabin Automata Translators'
year: '2013'
citekey: 'DBLP:conf/lpar/BlahoudekKS13'
used_sets:
- 'DBLP:conf/icse/DwyerAC99'
- 'DBLP:conf/cav/GastinO01'
- 'DBLP:conf/spin/GeldenhuysH06'
- 'DBLP:conf/spin/Pelanek07'
- 'DBLP:conf/cav/KretinskyE12'
---

# Abstract

The paper compares at the time of writing state-of-the-art translators. The paper evaluates the tool on three existing sets and adds three new formulas.

# Formulas

No new formulas (except randomly generated formulas) are listed.
