---
title: 'Patterns in Property Specifications for Finite-State Verification'
year: '1999'
citekey: 'DBLP:conf/icse/DwyerAC99'
---

# Abstract

Continuing the previous work in (@DBLP:conf/fmsp/DwyerAC98), this paper defines further generic patterns found in various specification logics.

# Formulas

The following formulas are listed, grouped by their pattern type:

* Precedence:
  * F p -> (~p U (s /\ ~p)
  * F r -> (~p U (s \/ r)
  * G ~q \/ F (q /\ (~p U (s \/ G ~p)))
  * G((q /\ F r) -> (~p U (s \/ r)))
  * G(q -> ((~p U (s \/ r)) \/ G ~p))

More can be found on the [Website (seems to be broken)](http://patterns.projects.cs.ksu.edu/documentation/patterns/ltl.shtml).

An alternative source is [genltl](https://spot.lrde.epita.fr/genltl.html) or the directory `data/formulas/literature` in the [Owl repository](https://owl.model.in.tum.de).  
