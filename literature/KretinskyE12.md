---
title: 'Deterministic Automata for the (F, G)-Fragment of LTL'
year: '2012'
citekey: 'DBLP:conf/cav/KretinskyE12'
used_sets:
- 'DBLP:conf/cav/SomenziB00'
- 'DBLP:journals/tcs/KleinB06'
- 'DBLP:conf/spin/Pelanek07'
---

# Abstract

The paper introduces a direct translation from a fragment of LTL to deterministic Rabin automata.

# Formulas

The following previouly unpublished formulas are defined:

* GF (Fa ∨ GFb ∨ FG(a ∨ b))
* FG (Fa ∨ GFb ∨ FG(a ∨ b))
* FG (Fa ∨ GFb ∨ FG(a ∨ b) ∨ FGb)
