---
title: 'The Blowup in Translating LTL to Deterministic Automata'
year: '2010'
citekey: 'DBLP:conf/mochart/KupfermanR10'
---

# Abstract

The paper closes the complexity gap for translating LTL to deterministic Büchi / Rabin automata, proving it to be exactly $2^2^(\Omega(n))$.

# Formulas

TODO
