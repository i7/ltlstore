---
title: 'Optimizing Büchi Automata'
year: '2000'
citekey: 'DBLP:conf/concur/EtessamiH00'
---

# Abstract

This paper introduces a family of optimizations for LTL to NBA translation and compares it to existing approaches.

# Formulas

No new formulas (except randomly generated formulas) are listed.
