---
title: 'Property specification patterns for finite-state verification'
year: '1998'
citekey: 'DBLP:conf/fmsp/DwyerAC98'
---

# Abstract

This paper introduces several generic patterns found in various specification logics.

# Formulas

The following formulas are listed, grouped by their pattern type:

* Absence:
  * G ¬p
  * F r -> ¬p U r
  * G (q -> G (¬ p))
  * G ((q /\ X F r) -> (¬p /\ X (~p U r)))
  * G (q -> (~p /\ X(~p U (r \/ G ~p))))
* Response:
  * G (p -> F s)
  * (p -> (~r U (s /\ ~r))) U (r \/ G ~r)
  * G (q -> G (p -> F s))
  * G ((q /\ X F R) -> (P -> (~r U (s /\ ~r))) U r)
  * G (q -> ((p -> (~r U (s /\ ~r))) U r) \/ G (p -> (~r U (s /\ ~r))))

More can be found on the [Website (seems to be broken)](http://patterns.projects.cs.ksu.edu/documentation/patterns/ltl.shtml).

An alternative source is [genltl](https://spot.lrde.epita.fr/genltl.html) or the directory `data/formulas/literature` in the [Owl repository](https://owl.model.in.tum.de).  
