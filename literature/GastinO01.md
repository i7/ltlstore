---
title: 'Fast LTL to Büchi Automata Translation'
year: '2001'
citekey: 'DBLP:conf/cav/GastinO01'
---

# Abstract

The paper introduces a new LTL to NBA translation via very weak alternating co-Büchi automata.

# Formulas

The following previouly unpublished formulas are defined:

* ¬((AND_{i=0}^n G F pi) → G(q → F r))
* ¬(p1 U (p2 U (... U pn)...)
