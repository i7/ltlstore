---
title: 'Rabinizer: Small Deterministic Automata for LTL(F,G)'
year: '2012'
citekey: 'DBLP:conf/atva/GaiserKE12'
used_sets:
- 'DBLP:conf/cav/SomenziB00'
- 'DBLP:journals/tcs/KleinB06'
- 'DBLP:conf/spin/Pelanek07'
- 'DBLP:conf/cav/KretinskyE12'
---

# Abstract

The paper presents Rabinizer, a tool for translating formulae of LTL(F,G) into deterministic Rabin automata. The paper evaluates the tool on three existing sets and adds three new formulas.

# Formulas

Nothing new.
