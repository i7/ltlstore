---
title: 'BEEM: Benchmarks for Explicit Model Checkers'
year: '2007'
citekey: 'DBLP:conf/spin/Pelanek07'
---

# Abstract

The paper collects several models with correctness properties (both safety and liveness) in LTL. The collection can be found at the archived [website](http://paradise.fi.muni.cz/beem/).

# Formulas

The following previouly unpublished formulas are defined:

* G(ac0 -> F gr0)
* G(active -> (standby R ((! noise) || standby)))
* G(a -> F(b || c))
* G(a -> F(b || c))
* G(dataout -> (zerotime /\ (oktime U datain1)))
* GF(b3sol || end) /\ GF(b3empty || end)
* ((G F dataok) /\ (G F nakok)) -> (G F consume)
* G(F correct)
* G(r0 -> (!p0 U (p0 U (!p0 U (p0 U (p0 /\ co))))))
* G(r1 -> (F(p1  /\  co)))
* G(r1 -> (!p1 U (p1 U (p1 /\  co))))
* G(r1 -> (!p1 U (p1 U (!p1 U (p1 U (p1 /\ co))))))
* G(res0 -> (! cend U (cend U (!cend  /\  (rt0 R !cend)))))
* G(res0 ->  (rt0 R !cend))
* G((sleeping0  /\  interrupton) -> ((!interrupton) R (sleeping0 || (!interrupton))))
* G((sleeping0  /\  sleepop0) -> ((!sleepop0) R (sleeping0 || (!sleepop0))))
* G(standby -> (apactivesend R ( (!active) || apactivesend)))
* G(waiting0 ->(F in_elevator0))
* G(want0 -> (! cend U (cend U (!cend  /\  (rt0 R !cend)))))
* (G!write11) -> (G !read11)
* G(write11 -> ((G !write10) || ((! read10) U (write10))))
* ((!moveup) U pressedup0) || G (! moveup)
* (pready U produce0) -> ((cready U consume0) || G cready)
* !(!(get || reg) U not)  /\  G(unreg -> !(!(get || reg) U not))

