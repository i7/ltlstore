---
title: 'Efficient Büchi Automata from LTL Formulae'
year: '2000'
citekey: 'DBLP:conf/cav/SomenziB00'
---

# Abstract

The paper presents an algorithm to translate LTL formulae into Büchi automata. For the evaluation it collects several folklore formulas (which are not attributed).

# Formulas

The formulas are folklore and not attributed.
The following previously unpublished formulas are defined:

* p U q
* p U (q U r)
* 􏰀¬(p U (q U r))
* GF p → GFq
* (F p) U (G q)
* (G p) U q
* ¬(FFp ↔ Fp)
* ¬(GFp → GFq)
* ¬(GFp ↔ GFq)
* p R (p ∨ q)
* (X(p) U X(q)) ∨ ¬X(p U q)
* (X (p) U q) ∨ ¬X (p U (p ∧ q)) G(p → Fq)∧
* ((X (p) U q) ∨ ¬X (p U (p ∧ q))) G(p → Fq)∧
* ((X(p) U X(q)) ∨ ¬X(p U q)) G(p → Fq)
* ¬G (p → X (q R r))
* ¬(GFp ∨ FGq)
* G(Fp∧Fq)
* Fp∧F¬p
* (X (q) ∧ r) R
* X (((s U p) R r) U (s R r))
* (G(q ∨ GFp) ∧ G(r ∨ GF¬p)) ∨G q ∨ G r
* (G(q ∨ FGp) ∧ G(r ∨ FG¬p)) ∨G q ∨ G r
* ¬((G(q ∨ GFp)∧G(r ∨ GF¬p)) ∨G q ∨ G r)
* ¬((G(q ∨ FGp)∧G(r ∨ FG¬p)) ∨G q ∨ G r)
* G (q ∨ X G p) ∧ G (r ∨ X G ¬p) G(q ∨ (Xp ∧ X¬p))
* (p U p) ∨ (q U p)