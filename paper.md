---
title: "LTL Store: Repository of LTL formulae from literature and case studies"
date: "May 2018"
author: "Jan Křetínský, Tobias Meggendorger, Salomon Sickert"
---

# Abstract

Theoretical analysis and determinitation of complexity bounds of newly developed algorihtms on LTL is often the first step to asses the practicallitay of it, but this needs to be complemented with a extensive experimental evaluation of formulas occuring in the literature and which are derived from case studies. This coninuesly extended technical report collects and compares commonly used formulae from the literature and provides them in a machine readable way.

The sources as well the formulas in machine readable can be found in the  [LTL Store repository](https://gitlab.lrz.de/i7/ltlstore).

# Formulas

Create automatically with includes?

## Synthetic

See (@DBLP:conf/tacas/WulfDMR08), (@DBLP:conf/lpar/BlahoudekKS13).

## Real-World Formulas

# Algorithm

Yeah, mathmode!

$$f(x)=x^2$$

# Other Cool Projects and Thanks to:

https://spot.lrde.epita.fr/man/genltl.1.html

# References