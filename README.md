# LTLStore

This repository gathers and categorizes various LTL formulas.
For now, the list of formulas is hard coded, but support for, e.g., parametrized families is planned for future releases.

## TODO

- Proper DB (.json / .yml files with description, references (standardized via DBLP link or URL?), etc.)
- Parametrized families (multiple parameters)
- Tagging
- Script outputting a list of formulas, e.g. script.py gr1(1-5) fairness(2,4) +literature,+small,-safety
- Generating website / pdf
- Run given tool on a parametric formula until it times out

## Further Reading

- The project is loosely inspired by [Buchi Store](buchi.im.ntu.edu.tw/index.php), a collection of Büchi automata
- The [Spec Patterns](http://patterns.projects.cs.ksu.edu/) webpage provides some additional ways of categorizing LTL formulas and contains typical representatives of several patterns